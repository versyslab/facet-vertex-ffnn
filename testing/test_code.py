import sys
sys.path.insert(0, '../class/')

import unittest
import os
import time
import nnet
import cubelattice as cl
import multiprocessing
from functools import partial
from scipy.io import loadmat
from scipy.io import savemat
from random import shuffle
import numpy as np

class TestStringMethods(unittest.TestCase):
    def test_one_dim_and(self):
        p,i, j = '2', 1, 2
        compute_unsafety = True
        print('Code test with the computation of unsafe input sets for Property 2 and Network1_2...\n')
        if p == '1':
            lb = [55947.691, -3.141592, -3.141592, 1145, 0]
            ub = [60760, 3.141592, 3.141592, 1200, 60]

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vs = np.dot(afv.vertices, afv.M.T)+afv.b.T
                    if np.any(vs[0, :] >= 3.9911):
                        safe = False
                        break
                return safe

        elif p == '2':
            lb = [55947.691, -3.141592, -3.141592, 1145, 0]
            ub = [60760, 3.141592, 3.141592, 1200, 60]
            unsafe_mat = np.array([[-1.0, 1.0, 0, 0, 0], [-1, 0, 1, 0, 0], [-1, 0, 0, 1, 0], [-1, 0, 0, 0, 1]])
            unsafe_vec = np.array([[0.0], [0.0], [0.0], [0.0]])

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmax(vertices, axis=1)
                    if np.any(indx == 0):
                        safe = False
                        break
                return safe

        elif p == '3':
            lb = [1500, -0.06, 3.1, 980, 960]
            ub = [1800, 0.06, 3.141592, 1200, 1200]
            unsafe_mat = np.array([[1.0, -1, 0, 0, 0], [1, 0, -1, 0, 0], [1, 0, 0, -1, 0], [1, 0, 0, 0, -1]])
            unsafe_vec = np.array([[0.0], [0], [0], [0]])

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if np.any(indx == 0):
                        safe = False
                        break
                return safe

        elif p == '4':
            lb = [1500, -0.06, 0, 1000, 700]
            ub = [1800, 0.06, 0.000001, 1200, 800]
            unsafe_mat = np.array([[1.0, -1, 0, 0, 0], [1, 0, -1, 0, 0], [1, 0, 0, -1, 0], [1, 0, 0, 0, -1]])
            unsafe_vec = np.array([[0.0], [0], [0], [0]])

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if np.any(indx == 0):
                        safe = False
                        break
                return safe

        elif p == '5':
            lb = [250, 0.2, -3.141592, 100, 0]
            ub = [400, 0.4, -3.141592 + 0.005, 400, 400]

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if np.any(indx != 4):
                        safe = False
                        break
                return safe

        elif p == '6.1':
            lb = [12000, 0.7, -3.141592, 100, 0]
            ub = [62000, 3.141592, -3.141592 + 0.005, 1200, 1200]

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if np.any(indx != 0):
                        safe = False
                        break
                return safe

        elif p == '6.2':
            lb = [12000, -3.141592, -3.141592, 100, 0]
            ub = [62000, -0.7, -3.141592 + 0.005, 1200, 1200]

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if np.any(indx != 0):
                        safe = False
                        break
                return safe

        elif p == '7':
            lb = [0, -3.141592, -3.141592, 100, 0]
            ub = [60760, 3.141592, 3.141592, 1200, 1200]

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if np.any(indx == 3) or np.any(indx == 4):
                        safe = False
                        break
                return safe

        elif p == '8':
            lb = [0, -3.141592, -0.1, 600, 600]
            ub = [60760, -0.75 * 3.141592, 0.1, 1200, 1200]

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if (2 in indx) or (3 in indx) or (4 in indx):
                        safe = False
                        break
                return safe

        elif p == '9':
            lb = [2000, -0.4, -3.141592, 100, 0]
            ub = [7000, -0.14, -3.141592 + 0.01, 150, 150]

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if np.any(indx != 3):
                        safe = False
                        break
                return safe

        elif p == '10':
            lb = [36000, 0.7, -3.141592, 900, 600]
            ub = [60760, 3.141592, -3.141592 + 0.01, 1200, 1200]

            def verification(outputs):
                safe = True
                for afv in outputs:
                    vertices = np.dot(afv.vertices, afv.M.T) + afv.b.T
                    indx = np.argmin(vertices, axis=1)
                    if np.any(indx != 0):
                        safe = False
                        break
                return safe

        else:
            raise RuntimeError(f"property {p} is not defined!")


        nn_path = "nnet-mat-files/ACASXU_run2a_" + str(i) + "_" + str(j) + "_batch_2000.mat"
        filemat = loadmat(nn_path)

        W = filemat['W'][0]
        b = filemat['b'][0]
        ranges = filemat['range_for_scaling'][0]
        means = filemat['means_for_scaling'][0]

        for n in range(5):
            lb[n] = (lb[n] - means[n]) / ranges[n]
            ub[n] = (ub[n] - means[n]) / ranges[n]

        nnet0 = nnet.nnetwork(W, b)
        nnet0.verification = verification
        nnet0.compute_unsafety = compute_unsafety
        nnet0.start_time = time.time()
        nnet0.filename = "logs/output_info_" + p + "_" + str(i) + "_" + str(j) + ".txt"
        if os.path.isfile(nnet0.filename):
            os.remove(nnet0.filename)

        initial_input = cl.cubelattice(lb, ub).to_lattice()
        cpus = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(cpus)
        m = multiprocessing.Manager()
        lock = m.Lock()

        unsafe_inputs = []
        nputSets0 = nnet0.singleLayerOutput(initial_input, 0)
        nputSets = []
        for apoly in nputSets0:
            nputSets.extend(nnet0.singleLayerOutput(apoly, 1))
        shuffle(nputSets)

        nnet0.unsafe_domain = [unsafe_mat, unsafe_vec]
        unsafe_inputs.extend(pool.imap(partial(nnet0.layerOutput, m=2), nputSets))
        unsafe_inputs = [item for sublist in unsafe_inputs for item in sublist]

        pool.close()
        correct_num =102
        # print('The correct number of safe inputs is: ', correct_num)
        # print('This test generates: ', len(unsafe_inputs))
        # if len(unsafe_inputs)==correct_num:
        #     print('\nPass!\n')
        # else:
        #     print('\nNot Pass!')
        #     print('*Caution*, a small number difference is usually due to the numerical error in operation systems\n')

        # print('The code test is done. The following reproduces results in the paper.\n')
        self.assertEqual(len(unsafe_inputs), correct_num)      

if __name__ == '__main__':

    unittest.main()